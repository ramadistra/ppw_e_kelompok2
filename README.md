# Informasi Deployment
[![pipeline status](https://gitlab.com/cahyanugraha12/ppw_e_kelompok2/badges/master/pipeline.svg)](https://gitlab.com/cahyanugraha12/ppw_e_kelompok2/commits/master)
[![coverage report](https://gitlab.com/cahyanugraha12/ppw_e_kelompok2/badges/master/coverage.svg)](https://gitlab.com/cahyanugraha12/ppw_e_kelompok2/commits/master)
* Link HerokuApp : https://ppw-e-kelompok2.herokuapp.com/

# Informasi Kelompok
* Kelas : PPW E
* Nomor Kelompok : 2
* Anggota :
    * Hardyin Alexander Immanuel Hutapea (1406565732)
    * Shahnaz Estee Laseidha (1606883146)
    * Pande Ketut Cahya Nugraha (1706028663)
    * Rhendy Rivaldo (1706039982)

# Informasi Project
Project yang dipilih adalah Crowd Funding, dengan pembagian tugas (berdasarkan spesifikasi di SceLe, tidak menutup kemungkinan berbeda dengan realita):
- Formulir pendaftaran jadi donatur - Rhendy
- Formulir submit donasi - Shahnaz 
- Halaman yang menampilkan program - Cahya
- Halaman yang menampilkan news atau berita - Hardyin

Perkiraan pembagian app menjadi :
- App Akun- Rhendy
- App Berita - Hardyin
- App Donasi - Cahya dan Shahnaz


# Development Guidelines
* Disarankan membuat branch untuk masing-masing fitur dengan nama app atau fitur yang dikerjakan
* Files static dan templates yang akan digunakan oleh beberapa app 
(yang kita sebut sebagai common static/template) diletakkan di common_files/static dan common_files/template
* Syntax untuk penggunaan common static 
    > **{% static 'styles/nama_folder/nama_file %}**
* Penggunaan common templates cukup tuliskan namanya saja
* Static dan templates yang hanya digunakan oleh satu app diletakkan di nama_app/static/nama_app dan nama_app/templates/nama_app. Penting untuk mencegah bentrokan nama file.
* Syntax untuk penggunaan static app
    > **{% static 'nama_app/styles/nama_folder/nama_file %}**
* Penggunaan templates app
    > **'nama_app/nama_template'** 