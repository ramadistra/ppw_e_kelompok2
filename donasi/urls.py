from django.urls import re_path
from donasi.views import hello_world

app_name = 'donasi'

urlpatterns = [
    re_path(r'^$', hello_world, name='hello_world'),
]